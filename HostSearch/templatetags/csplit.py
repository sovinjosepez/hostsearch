import re
from django import template

register = template.Library()

@register.filter(name='csplit')
def csplit(value):
    """
        Returns the value turned into a list.
    """
    try:
        s = value.decode("utf-8")
        lis = s.split('\n')
        return lis
    except:
        return value
