from django.shortcuts import render, HttpResponse
from django.views.generic import View
from .utils import HosOperations, validate_host_name


class GetTrace(View):

    def get(self, request):
        return HttpResponse('test')


class HomePageView(View):

    def get(self, request):
        """
        Render Html base page
        :param request:
        :return:
        """
        return render(request, "base.html", {'status': False})

    def post(self, request):
        url = request.POST.get('host_name', '').strip()
        command = request.POST.get('command', 'ping').strip()
        status = validate_host_name(url)
        if not status:
            return render(request, "base.html", {'status': False, 'e_status': True})
        host = url.split('//')[1]
        h =  HosOperations(host, command)
        t_res = h.get_result()
        context = {
            't_res': t_res,
            'status': True,
            'e_status': False,
            's_word': url,
            'command': command
        }
        return render(request, "base.html", context)

