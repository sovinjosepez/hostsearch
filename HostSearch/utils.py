try:
    import urllib.request as urlrequest
except ImportError:
    import urllib2 as urlrequest
import subprocess
from multiprocessing.dummy import Pool as ThreadPool

class HosOperations:

    def __init__(self, url, command):
        self.url = url
        self.command = command

    def ping_to_host(self, url):
        """
         Method for Ping a URL
        :param url:
        :return:
        """
        try:
            proc = subprocess.Popen(["ping", "-c 5", url], stdout=subprocess.PIPE).communicate()[0]
            return proc
        except Exception as e:
            return e

    def trace_host(self, url):
        """
         Method For execute a Command, command will run in 15 thread
        :param url:
        :return:
        """
        try:
            if 'ping' in self.command and not '-c' in self.command:
                self.command = 'ping -c 10'
            proc = subprocess.Popen('%s %s' % (self.command, url), stdout=subprocess.PIPE, shell=True).communicate()[0]
            return proc
        except Exception as e:
            return e

    def get_result(self):
        """
         Method for trigger the thread
        :return:
        """
        pool = ThreadPool(15)
        #presults = pool.map(self.ping_to_host, [self.url]*5)
        tresults = pool.map(self.trace_host, [self.url]*15)
        pool.close()
        pool.join()
        return tresults


def validate_host_name(url):
    """
     Method for validate URL
    :param url:
    :return:
    """
    try:
        urlrequest.urlopen(url, timeout=15)
        return True
    except Exception as e:
        return False